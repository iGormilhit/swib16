# SWIB 16, les notes

Les notes de SWIB 16.

En lien avec le workshop, il y a deux dépôts `git`, dans un autre dossier.

* [L'atelier](workshop-2016-11-28.md)
* [Le premier jour](notes-2016-11-29.md)
* [Le deuxième jour](notes-2016-11-30.md)
