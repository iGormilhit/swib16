---
title: SWIB16
subtitle: les notes du jour 2
author: mii
date: 2016-11-29
update: 2016-12-11
---

**Table des matières**

- [(Packaged) Web Publications](#packaged-web-publications)
    - [Questions](#questions)
- [Dutch WW2 underground newspapers](#dutch-ww2-underground-newspapers)
    - [questions](#questions)
- [Improving data quality at Europeana](#improving-data-quality-at-europeana)
    - [questions](#questions-1)
- [Linked Open Data in Practice: Emblematica Online](#linked-open-data-in-practice-emblematica-online)
    - [questions](#questions-2)
- [RDF by Example: rdfpuml for True RDF Diagrams, rdf2rml for R2RML Generation](#rdf-by-example-rdfpuml-for-true-rdf-diagrams-rdf2rml-for-r2rml-generation)
- [Towards visualizations-driven navigation of the scholarship data](#towards-visualizations-driven-navigation-of-the-scholarship-data)
- [TIB|AV-Portal - Challenges managing audiovisual metadata encoded in RDF](#tibav-portal-challenges-managing-audiovisual-metadata-encoded-in-rdf)
    - [questions](#questions-3)
- [Implementing the IIIF Presentation 2.0 API as a Linked Open Data Model in the Fedora Repository](#implementing-the-iiif-presentation-20-api-as-a-linked-open-data-model-in-the-fedora-repository)
- [Lightning sessions](#lightning-sessions)
- [URI to MARC](#uri-to-marc)
    - [outils](#outils)

[D'abord les mots des institutions qui accueillent et/ou qui financent.]

# (Packaged) Web Publications

Ivan Herman, W3C, [support de présentation](https://w3c.github.io/dpub/2016/SWIB/)

Importance de la publication numérique, incontournable.

Standard de fait : ePub 3 (3.1 pour être précis). Pas seulement les livres, mais aussi revues, *conference proceedings*, JEP (*Journal of Electronic Publishing*), rapports, ...

ePub supporté par Google Docs, Apple Pages, LibreOffice, Microsoft Edge, ... Format basé sur les technologies du Web : HTML, CSS, SVG, Javascript (à vérifier pour le javascript). L'ePub est conforme à l'*Open Web Platform*. La publication numérique entretient un rapport étroit avec l'*OWP*, lien qui doit être renforcé. Mais, il s'agit également de lier plus étroitement la publication numérique et le Web.

Le Web est caractérisé par un certain nombre de manques du point de vue de la publication numérique, comme la pagination, ou les transitions graphiques, ou encore la structuration de l'info. À l'inverse, il existe des *libraries* disponibles pour le Web qui ne sont pas utilisées, ou pas suffisamment, dans le contexte de la publication numérique.

*Digital Publication Interest Group*.

Le livre, au sens d'une information structurée qui forme une unité, devrait être indépendant du format, ce qui n'est pas encore le cas. C'est d'autant plus criant lorsque l'on passe d'une version Web d'un livre, à sa version ePub, ou PDF. La situation empire lorsque l'on passe du mode en ligne au mode  déconnecté. Enin, lorsqu'il s'agit d'opérer des transitions entre appareils, entre un mode connecté ou non connecté, alors ça devient très difficile : les signets qui ne suivent pas forcément, ni les annotations, ni la disponibilité des outils de navigation (table des matières, index), des hyperliens, voir des contenus dynamiques (médias).

La publication académique numérique se fait en ligne, mais pour un usage déconnecté (mais pas en pdf !). De plus, un article est le plus souvent multimedia. Il contient des données, peut-être des graphiques, peut-être que les graphiques sont dynamiques, du code, des vidéos, ... Dès lors, pour la  publication académique ne se ferait-elle pas seulement sur le Web ? Il y a malheureusement des choses que le Web ne sait pas encore faire.

Tous les éléments constituant une publication sur le Web sont lié, mais ce sont des éléments indépendants qui possèdent une URI propre. Or, une des caractéristique d'un livre ou d'un article est son unité. Il est donc nécessaire de pouvoir pointer avec une seule URI à un ensemble de ressources qui forment une unité. C'est nécessaire pour l'archivage, le dépôt légal par exemple. Il faut pourvoir aussi vérifier l'intégriter de la publication, en facilité le partage, ce qui soulève des questions de fiabilité et d'interopérabilité. Sont nécessaires aussi des outils de recherche, d'analyse textuelles, des numérotations des pages, des notes, des sections, ...

Défi d'architecture : gestion du online/offline. Mettre dans un cachee local, et lorsque c'est nécessaire faire une requête sur le Web, ponctuellement, si la connexion est disponible. C'est faisable et en train d'être réalisé, au moyen des *Web Worker* et *Service Worker*. *Work In Progress*.

Le présentateur donne un exemple de livre qu'il lit sur sa machine (laptop), et qui, au fur et à mesure de la lecture, devient  accessible en mode déconnecté, avec tout le côté dynamique du html.

PWP [*(Packaged) Web Publication*] : la réunion en un ensemble, en une unité, est nécessaire par exemple pour l'archivage, mais pour bien d'autres éléments également. L'ensemble de l'industrie de la publication est basée sur cette untié. Pour la réaliser dans le domaine de la publication numérique, on arrive assez facilement à la solution de l'ePub, ou quelque chose de  similaire. On peut par exemple, remplacer le XML par du JSON, Mais l'idée est là : à savoir des métadonnées d'administration, des instructions de rendu, et du contenu, à l'image de l'ePub. Et il faut ajouter un moteur *PWP* pour gérer la transition entre le mode connecté et le mode déconnecté.

Pour ce *PWP*, il y a un certain nombre d'éléments en matière d'ergonomie à améliorer, comme la pagination qui est souvent nécessaires dans le cas de la lecture d'un livre, ou d'un support de présentation. Il faut également trouver le moyen de permettre une certaine personnalisation, comme c'est le cas pour l'ePub, mais encore difficilement et peu souvent sur le Web : changer les couleurs, les polices de caractères, les tailles, etc.

Le *PWP* n'est pas censé remplacer l'ePub. Pour le moment, *PWP* est une idée, une réalisation future. Du point de vue des éditeur, l'ePub et le *PWP* sont très similaires.

L'ePub a été développé par l'[IDPF](http://idpf.org/ "International Publishing Forum"), le Web plutôt par le W3C. Actuellement il y a un rapprochement entre ces deux organisations concernant la publication nurmérique. Il est prévu qu'elles mergent en 2017, ce qui pourrait profiter passablement aux éditeurs, qui disposeront de l'ensemble des outils développés pour le Web. Le Web aussi va s'enrichir de cette collaboration et s'informer de l'expérience des éditeurs en matière d'ergonomie, de typographie, de processus, etc.

## Questions

En matière d'annotations, RFC est en attente pour février 2017. Il mentionne [hypothesis](https://hypothes.is/), mais en *open source* et avec une architecture plus distribuée.

Quant au lien avec les données liées, il rappelle qu'on peut, du moins depuis la version 3.1 de l'ePub, mettre n'importe quelle données dans le *PWP*, par exemple du RDF, des microdata, etc. Mais avec le *PWP*, les publications numériques intègrent vraiment le Web, ce qui ouvre la porte à des application LOD.

# Dutch WW2 underground newspapers

Olaf Janssen / Gerard Kuys

Illegal resistance WW2 newspaper. De très grandes à très petites éditions. Environ 1'300 *titres*. Disponibles et décrites dans un catalogue.

2010 : numérisation. Dispo sur le Web sur Delpher/kranten. OCR. Alto (?). Mais soulève des questions de type contextuel : radial ou non, etc. Ne peuvent être répondu via Delpher, pas d'info. contextuelles.

→ Wikipedia. Difficulté : les données viennent de sources multiples : catalog, delpher, wp.

Et seul. quelques titres ont leur page wp. Passer de 15 à 1300.

*Wikiproject* : décrire et lier les données sur les newspaper. Utilise wp pour atteindre une large audience + nature ouverte de wp → réutilisations possibles.

Comment obtenir de l'info contextuelle ? Parfois des monographies, notamment une sur l'ensemble des titres. Ce livre donne des ID à chaque titre et lieu d'édition. Peut être relié. Liens avec étudiants (auteur du titre exemple), liens avec d'autres titres...

Libérer l'info du book. Scanné, OCR, gestion du © → en CC-BY-SA en ligne. Puis convertir le PDF en une database structurée. Liens avec le catalog et Delpher, puis sources externes.

Ont fait une db LOD !

du coup, ont généré des pages wp, puis export sur wikidata, dbpedia, dataviz.

Importance de générer des pages *uniformes*, standardisées → templates pour générer des articles stubs. Puis enrichis pas intervention humaine, crowdsource, par des volontaires wp (8 ou 10), et ça avance petit à petit.

[very cool]

## questions

certains titres n'ont pas leur propre page, pas assez d'info contextuel.

la réaction de la communauté wp : ont communiqué dès le début avec la com. wp pour savoir si c'est au niveau exigé.

aussi des articles sur les auteurs

# Improving data quality at Europeana

Péter Király / Hugo Manquihas / ...

Plusieurs partenaires.

Montre le data workflow. Collecte des schema de metadata : dublincore, EAD, MARC, etc. Puis agréation, acquisition, transformation dans le EDM (europeana data model).

Difficulté de mesurer la qtité de bonne et mauvaise metadata.

Trouver des moyens d'identifier les mauvaises metadata. Améliorer les données permet à celles-ci d'être cherchées, utilisées.

Trouver des métrics. Être capable de les implémenter. But : identifier la qualité. [en très gros]

## questions

Possible d'utiliser ce processus dans d'autres contextes.

Metrics de provenance : pas encore introduit, mais ils ont l'intention de le faire. Identifier les meilleures sources.

# Linked Open Data in Practice: Emblematica Online

 Myung-Ja K. Han / Timothy W. Cole / Maria Janina Sarol / Patricia Lampron / Mara Wade / Thomas Stäcker / Monika Biel

Emblem : genre literaire, text et images, très connecté au contexte historique, avec une iconographie de sources très différences

Emblemetica Online : portal sur la renaissance, avec une collections numérisées, 1400 livres et 28000 emblems. Avec un accès précis, via SPINE

SPINE : a model metadata, SPINE et spinexmlschema. SPINE est un container de différents model de description.

Granularité : livre, emblems, image, sélection dans l'image.

Mettre en liens avec autres réservoirs de données, bien entendu. Mettre à disposition des uri permanente pour chaque niveau de granularité mais aussi pour l'ensemble d'une œuvre. Utilise schema.org et RDFa. Pour schema.org propose une extension pour les emblem. Via schema.org espère être accessible via Google.

Les liens qu'ils trouvent, par exemple vers BnF ou VIAF, sont enregistrés dans leur model, donc mis à disposition des users via l'interface Web.

Metadata quality matters. Ah ?

La résolution automatique des metadonnées demande encore une intervention humaine.

Tous les services Linked Data ne sont pas également ouverts, utiles, similaires.

## questions

schema.org → s'exprime de manière différente, pouquoi RDFa ?

Pour l'instant que de l'accès et de la découverte. Mais espère que dans le futur les chercheurs peuvent en faire plus.

# RDF by Example: rdfpuml for True RDF Diagrams, rdf2rml for R2RML Generation

Vladimir Alexiev

Data modeling, besoin d'un outil de visualisation → comprendre les données. Sur la bse du RDF.

Présente sont outil, sa syntaxe pour construire des graph à partir de données sémantiques.

[plantUML]

Apparemment utilise ces graphs pour convertir les relations et les données dans d'autres modèles, comme par exemple RDF.

#  Towards visualizations-driven navigation of the scholarship data

Christina Harlow / Muhammad Javed / Sandy Payette

Visualiser la production académique de Cornell. Mais aussi d'obtenir les données, les manipuler...

Mesurer l'impact de la recherche de Cornell.

Implique bib, académic dept, students et l'université. Obtenir les stats de consultations des revues, la liste des revues, si c'est decrit dans le catalogue...

Dpt: nbre de publication, les grants, collaborations entre dept., quelles sont les recherches ?

Students: trouver les experts dans tel ou tel domaine, qui choisir comme superviseur...

Uni: impact.

Scholars@Cornell

[à creuser!]

# TIB|AV-Portal - Challenges managing audiovisual metadata encoded in RDF

Jörg Waitelonis

2 ans. Portail sur des vidéos. Open: les users peuvent uploader des contenus. Véfification des droits et de la qualité des données. Puis analyse des vidéos, speech2text, segmentation, ocr, visual concept detection.

Possibilité de chercher, citer des segments (doi).

Toutes les metadonnées sont en RDF (schema, bibframe, etc.).

NIF : NLP interchange format

tout accessible /opendata

Améliore l'ocr avec des liens vers des ontologies ou autorités.

Tâche : identifier les termes à lier, et à quelle entité le relier. Ressemble à l'annotation en bioinformatique. Humains mauvais en complétude (rappel), mais bon en précision, les machines l'inverse. Donc identifier avec machine, vérifier le lien avec humain ? Rappelle aussi bioinformatique.

Machine observe l'annotation manuelle pour s'améliorer.

## questions

Sélectionne les sous partie de la GND en lien avec le sujet de la vidéo. Pas pris en compte d'autres fournisseurs d'entités.

Dispose d'env. 50% de la GND qu'ils utilisent en trad.

# Implementing the IIIF Presentation 2.0 API as a Linked Open Data Model in the Fedora Repository

Christopher Hanna Johnson

Lance une vidéo... C'est sa présentation. Avec un son terrible, le souffle et tout.

# Lightning sessions

Canada : Migration d'AACR2 à RDA, puis de MARC à bf: [Bibframe]

LOC-DB : Linked Open Citation DB. Cataloguing citations. De manière identique à l'ingestion et description des livres, revues...

LDN : Linked Data Notifications (W3C).

Ne constate pas de volonter de remplacer MARC par Bibframe à un niveau global ou européen. DK, implémente RDA, puis Bibframe.

# URI to MARC

Le tout c'est pas d'avoir des URI dans le MARC, mais aussi d'avoir des moyens d'utiliser ces données.

A quel niveau connecter : le champs, sous-champs, tous les sous-champs ? Pas suffisamment d'éléments pour savoir comment connecter via une URI dans un sous-champs zéro deux champs ou sous-champs MARC.

Utiliser catmandu en dehors du système, extraire, travailler, importer. Ou utiliser catmandu pour enrichir MARC. Mentionne aussi Metafacture, d:swarm. Le problème est pas technique, mais il faut le faire. L'important est d'accéder aux data authorities. Importance de Opendata, de ce point de vue.

Mais à quelle autorité, à quelle notice pointer ? Quelle est la notice qui soit durable ? Le mapping dans VIAF des id national n'est pas stable.

## outils

catmandu, metafacture, il y en a des centaines, la disponibilité des outils, ce n'est pas le souci. L'idée de ces outils, c'est d'extraire des données, les traiters, écrire dans des fichiers.

Mentionne MarcEdit. OpenRefine. **question à johnny : invenio** : traiter les données

! invenio-pid-store !

Idée : avons-nous encore besoin d'un fichier d'autorité. Mais le jour où tout est LOD, triplettes, données, alors plus besoin de records.

Fonction des fichiers d'autorités : id et résoudre l'ambiguité. Mais un records est plus qu'une collection de triplets.

Utilité pour les users ? Persons. Lien entre la ressources électronique et papier, pour avoir l'un quand l'autre n'est pas disponible. Le lien est fait automatiquement si on catalogue la version électronique avec un template.

Importance de liens entre données bibliogr et données extérieures.
